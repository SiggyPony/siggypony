import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import '../node_modules/bootstrap/dist/css/bootstrap.css';

class App extends Component {
  render() {
    return (
      <Grid>
        <Row>
          <Col style={{ paddingTop: '5em' }}>
            <div style={{ color: '#FFF', fontSize: '3em' }}>
              Siggypony.net <img src="./stawberry.png" style={{ height: "100px", width: "auto" }} /><img src="./pony-head.png" />
            </div>
          </Col>
        </Row>
        <Row>
          <Col md={3} mdOffset={2} style={{ paddingTop: '8em' }}>
            <img src="me.jpg" />
          </Col>
          <Col md={6} style={{ paddingTop: '10em' }}>
            <div style={{ color: '#FFF' }}>
              Moopony!!!!  Opps, someone left the lights on...  <br />
              However, it does indeed appear the pony isn't actually really in the building....< br />< br />
              She's been so busy coding for other people she has largely neglected her website :'(
            </div>
            < br />
          </Col>
        </Row>
        <Row>
          <Col md={9} mdOffset={2} style={{ paddingTop: '3em' }}>
            <div style={{ fontSize: '2em', paddingTop: '1em', color: '#FFF' }}>
              About the Siggy < br />
            </div>
            <div  style={{ paddingTop: '.5em', color: '#FFF' }}>
              Name: Sigourney Juneau< br />
              Occupation: Software developer< br />
              LinkedIn Profile: < br /><a href="https://nz.linkedin.com/in/sigourney-juneau-44ab2a6b">https://nz.linkedin.com/in/sigourney-juneau-44ab2a6b</a>< br />< br />
              Bitbucket: < br /><a href="https://bitbucket.org/SiggyPony/">https://bitbucket.org/SiggyPony/</a> (Sorry not much interesting public on there)< br />< br />
              Likes: Ponies, Software development, writing debugging code and git commits about ponies when no one is looking (and breaking old installs of bitbucket with pony emoji's), Cycling, Sleeping, Watching youtube, Learning new stuff, all cute and colourful things.< br />


            </div>
            < br />
            <div style={{ fontSize: '2em', paddingTop: '1em', color: '#FFF' }}>
              Portfolio
            </div>
            <div  style={{ paddingTop: '.5em', color: '#FFF' }}>
              Unfortunately most if not all of my personal work is private and not opensourced for various reasons, and the majority of my coding happens for work (surprise!!)
              I still do some website development though which I can link too< br />
              More to be added soon< br />< br />
              <a href="http://emmelinestory.co.uk/">http://www.emmelinestory.co.uk/</a>< br />
              <a href="http://www.jrmanawa.com/">http://www.jrmanawa.com/</a>< br />
              <a href="http://theexpeditionists.world/">http://theexpeditionists.world/</a>< br />
              < br />
            </div>
            <div style={{ fontSize: '1.5em', paddingTop: '1em', color: '#FFF' }}>
              Other projects
            </div>
            <div  style={{ paddingTop: '.5em', color: '#FFF' }}>
              My currently active personal projects include,< br />< br />
              A farming game written in Android.< br />
              My alarm clock (An embedded systems project)< br />
              < br />
            </div>

            < br />
            <div style={{ fontSize: '2em', paddingTop: '1em', color: '#FFF' }}>
              The pony pen
            </div>
            <div  style={{ paddingTop: '.5em', color: '#FFF' }}>
              Things have changed drastically since I took this photo and its now so messy I don't want to take another one.< br />< br />
              My workstation for development is a Phenom X6 1035T with 8Gb DDR3 1333 Ram and a Nvidia GTX470. < br />
              I try keep my development work isolated from my non-development life so I have an entirely separate computer setup
              next to my workstation which I use for Music, videos etc while developing which is on a third 22" monitor.
              It's a Phenom X4 840 with 8Gb DDR-800 Ram and it has the GTS-250 which was originally inside my workstation.
              <img src="workspace.jpg" style={{ width: '100%', height: 'auto', paddingTop: '2em' }}/>< br />
            </div>
            < br />
            <div style={{ fontSize: '1.8em', paddingTop: '1em', color: '#FFF' }}>
              My babies (workstation computers) :P with favourite cases EVER = Cold feet from Airflow< br />
              <img src="babies.jpg" style={{ width: '80%', height: 'auto', paddingTop: '2em' }}/>< br />
            </div>

            <div style={{ fontSize: '2em', paddingTop: '1em', color: '#FFF' }}>
              That's all from Siggy. Check back soon...
            </div>
            <div  style={{ paddingTop: '.5em', paddingBottom: '4em', color: '#FFF' }}>
             or email <a href="mailto:siggypony@siggypony.net">siggypony@siggypony.net</a> and say a thing.
            </div>
          </Col>
        </Row>
      </Grid>
    );
  }
}

export default App;
